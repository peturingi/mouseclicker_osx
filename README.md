MouseClicker
============

Mouse Clicker for OS X 10.9+

Clicks the Mouse automatically.

![screenshot](https://raw.githubusercontent.com/PeturIngi/MouseClicker/master/docs/screenshot.png)

License
=======
© 2014 EP Software. All rights reserved.

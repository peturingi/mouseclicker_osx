#import "HotKeyController.h"

@implementation HotKeyController

- (id)init {
    self = [super init];
    NSAssert(self, @"Failed to init.");
    return self;
}

- (void)awakeFromNib {
    [self registerHotKey];
}

- (void)registerHotKey {
    EventTypeSpec eventType;
    eventType.eventClass=kEventClassKeyboard;
    eventType.eventKind=kEventHotKeyPressed;
    
    EventHotKeyID hotKeyId;
    hotKeyId.signature='htk1';
    hotKeyId.id=1;
    
    EventTargetRef targetRef = GetApplicationEventTarget();
    EventHotKeyRef hotKeyRef;
    
    RegisterEventHotKey(1, cmdKey+optionKey, hotKeyId, targetRef, 0, &hotKeyRef);
    InstallApplicationEventHandler(&hotKeyHandler,1,&eventType,(__bridge void *)self,NULL);
}

OSStatus hotKeyHandler(EventHandlerCallRef nextHandler,EventRef theEvent, void *userData) {
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:HOT_KEY_NOTIFICATION object:nil]];
    return noErr;
}

@end
#import "MouseManager.h"

#define KVC_SPEED   @"speed"
#define MIN_SPEED   1
#define MAX_SPEED   50

@implementation MouseManager

- (id)init {
    self = [super init];
    if (self) {
        [self loadDefaultSpeed];
        mouse = [[Mouse alloc] init];
        [self registerForHotKeyNotification];
        [self registerSpeedObserver];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:self.startStopButton.bounds
                                                                options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp)
                                                                  owner:self userInfo:nil];
    [self.startStopButton addTrackingArea:trackingArea];
}

- (void)mouseEntered:(NSEvent *)theEvent {

    skipMouseClick = YES;
}
- (void)mouseExited:(NSEvent *)theEvent {

    skipMouseClick = NO;
}

- (void)loadDefaultSpeed {
    [self willChangeIncreaseAndDecreaseSpeed];
    self.speed = [self defaultSpeed];
    [self didChangeIncreaseAndDecreaseSpeed];
}

- (void)willChangeIncreaseAndDecreaseSpeed {
    [self willChangeValueForKey:@"canIncreaseSpeed"];
    [self willChangeValueForKey:@"canDecreaseSpeed"];
}

- (NSNumber *)defaultSpeed {
    return @(1);
}

- (void)didChangeIncreaseAndDecreaseSpeed {
    [self didChangeValueForKey:@"canIncreaseSpeed"];
    [self didChangeValueForKey:@"canDecreaseSpeed"];
}

- (void)registerForHotKeyNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleClicking:) name:HOT_KEY_NOTIFICATION object:nil];
}

- (void)registerSpeedObserver {
    [self addObserver:self
            forKeyPath:KVC_SPEED
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:KVC_SPEED]) {
        [self willChangeIncreaseAndDecreaseSpeed];
        [self didChangeIncreaseAndDecreaseSpeed];
    }
}

- (BOOL)canDecreaseSpeed {
    NSAssert(_speed != nil, @"speed is nil.");
    return self.speed.integerValue > MIN_SPEED && !self.isClicking;
}
- (BOOL)canIncreaseSpeed {
    NSAssert(_speed != nil, @"speed is nil.");
    return self.speed.integerValue < MAX_SPEED && !self.isClicking;
}

- (IBAction)increaseSpeedByOne:(id)sender {
    if (self.speed.integerValue < MAX_SPEED) {
        self.speed = @(self.speed.integerValue + 1);
    }
}
- (IBAction)decreaseSpeedByOne:(id)sender {
    if (self.speed.integerValue > MIN_SPEED) {
        self.speed = @(self.speed.integerValue - 1);
    }
}

- (IBAction)toggleClicking:(id)sender {
    self.isClicking = !self.isClicking;
    static NSTimer *timer;
    
    if (timer == nil) {       
        [self.startStopButton setTitle:NSLocalizedString(@"Stop", nil)];
        NSTimeInterval timeBetweenClicks = 1.0f / self.speed.integerValue;
        timer = [NSTimer scheduledTimerWithTimeInterval:timeBetweenClicks target:self selector:@selector(simulateMouseClick) userInfo:nil repeats:YES];
        [timer setTolerance:(timeBetweenClicks / 10.0f)];
    }
    else {
        [self.startStopButton setTitle:NSLocalizedString(@"Start", nil)];
        [timer invalidate];
        timer = nil;
    }
}

- (void)simulateMouseClick {
    if (!skipMouseClick) {

        if (_buttonSelected == LeftMouseButton) {
            [mouse clickWith:LeftMouseButton];
        }
        else if (_buttonSelected == RightMouseButton){
            [mouse clickWith:RightMouseButton];
        }
        else {
            NSString *errMsg = [NSString stringWithFormat:@"Invalid mouse button value: %ld", (long)self.buttonSelected];
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:errMsg userInfo:nil];
        }
    }
    else {
        // Do nothing
    }
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:KVC_SPEED];
}

@end
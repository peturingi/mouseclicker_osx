#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[]) {
    int returnValue;
    @try {
        returnValue = NSApplicationMain(argc, (const char **)argv);
    }
    @catch (NSException *exception){
        NSLog(@"Uncauched exception: %@", [exception callStackSymbols]);
        @throw;
    }
    return returnValue;
}

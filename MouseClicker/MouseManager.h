#import <Foundation/Foundation.h>
#import "Mouse.h"

@interface MouseManager : NSObject {
    Mouse *mouse;
    BOOL skipMouseClick;
}

@property (atomic) BOOL isClicking;
@property (atomic) NSInteger buttonSelected;
@property (strong, atomic) NSNumber *speed;
@property (atomic, readonly) BOOL canIncreaseSpeed;
@property (atomic, readonly) BOOL canDecreaseSpeed;
@property (assign) IBOutlet NSButton *startStopButton;


@end
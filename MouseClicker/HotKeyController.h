#import <Carbon/Carbon.h>

@interface HotKeyController : NSObject

OSStatus hotKeyHandler(EventHandlerCallRef nextHandler,EventRef theEvent, void *userData);

@end

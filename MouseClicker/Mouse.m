#import "Mouse.h"

@implementation Mouse

- (void)clickWith:(MouseButton)button {
    [self prepareForClick];

    switch (button) {
        case LeftMouseButton:
            [self leftClick];
            return;
            
        case RightMouseButton:
            [self rightClick];
            return;
    }
    
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:[NSString stringWithFormat:@"Invalid mouse button %u", button] userInfo:nil];
}

- (void)prepareForClick {
    static CGEventRef cursorEvent;
    
    cursorEvent = CGEventCreate(NULL);
    cursorLocation = CGEventGetLocation(cursorEvent);
    CFRelease(cursorEvent);
    source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
}

- (void)leftClick {
    [self leftDown];
    [self leftUp];
}

- (void)rightClick {
    [self rightDown];
    [self rightUp];
}

#pragma mark - Mouse Buttons

#pragma mark Left

- (void)leftDown {
    CGEventRef theEvent = CGEventCreateMouseEvent(source, kCGEventLeftMouseDown, cursorLocation, kCGMouseButtonLeft);
    CGEventSetType(theEvent, kCGEventLeftMouseDown);
    CGEventPost(kCGHIDEventTap, theEvent);
    CFRelease(theEvent);
}

- (void)leftUp {
    CGEventRef theEvent = CGEventCreateMouseEvent(source, kCGEventLeftMouseUp, cursorLocation, kCGMouseButtonLeft);
    CGEventSetType(theEvent, kCGEventLeftMouseUp);
    CGEventPost(kCGHIDEventTap, theEvent);
    CFRelease(theEvent);
}

#pragma mark Right

- (void)rightDown {
    CGEventRef rightEvent = CGEventCreateMouseEvent(source, kCGEventRightMouseDown, cursorLocation, kCGMouseButtonRight);
    CGEventSetType(rightEvent, kCGEventRightMouseDown);
    CGEventPost(kCGHIDEventTap, rightEvent);
    CFRelease(rightEvent);
}

- (void)rightUp {
    CGEventRef leftEvent = CGEventCreateMouseEvent(source, kCGEventRightMouseUp, cursorLocation, kCGMouseButtonRight);
    CGEventSetType(leftEvent, kCGEventRightMouseUp);
    CGEventPost(kCGHIDEventTap, leftEvent);
    CFRelease(leftEvent);
}

@end
#import <Foundation/Foundation.h>

typedef enum { LeftMouseButton = 0, RightMouseButton = 1 } MouseButton;

@interface Mouse : NSObject {
    CGPoint cursorLocation;
    CGEventSourceRef source;
}

/** @throw NSInvalidArgumentException
 */
- (void)clickWith:(MouseButton)button;

@end
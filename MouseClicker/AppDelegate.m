#import "AppDelegate.h"

@implementation AppDelegate

- (void)windowWillClose:(NSNotification *)notification {
    [NSApp terminate:nil];
}

@end
